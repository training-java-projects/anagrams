package ua.com.foxminded.anagrams;

public class Anagram {
    final String WORD_DELIMITER = " ";
    
    public String reverseSentence(String sentence) {
        String[] word = sentence.split(WORD_DELIMITER);
        StringBuilder reversedString = new StringBuilder();
        for (int i = 0; i < word.length; i++) {
            word[i] = reverseWord(word[i]);
        }
        for (int i = 0; i < word.length; i++) {
            reversedString.append(word[i]);
            if (i < word.length - 1) {
                reversedString.append(" ");
            }
        }
        return new String(reversedString);
    }

    private String reverseWord(String word) {
        char[] wordChars = word.toCharArray();
        char[] charsBuffer = new char[wordChars.length];
        for (int i = 0, j = 0; i < word.length(); i++) {
            if (Character.isAlphabetic(wordChars[wordChars.length - 1 - i])) {
                charsBuffer[j] = wordChars[wordChars.length - 1 - i];
                j++;
            }
        }
        for (int i = 0, j = 0; i < wordChars.length; i++) {
            if (Character.isAlphabetic(wordChars[i])) {
                wordChars[i] = charsBuffer[j];
                j++;
            }
        }
        return new String(wordChars);
    }
}