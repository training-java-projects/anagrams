package ua.com.foxminded.anagrams;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) throws IOException {
        Anagram anagram = new Anagram();
        Scanner in = new Scanner(System.in);
        System.out.print("Input sentence is: ");
        String sentence = in.nextLine();
        String reversedSentence = anagram.reverseSentence(sentence);
        System.out.println("Output sentence is: " + reversedSentence);
    }
}